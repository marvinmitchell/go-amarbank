package logger

import (
	"bytes"
	"encoding/json"
	"net"
	"net/http"
	"os"
	"strings"
	"time"
)

var netClient = &http.Client{
	Timeout: 30 * time.Second,
	Transport: &http.Transport{
		Dial: (&net.Dialer{
			Timeout:   10 * time.Second,
			KeepAlive: 30 * time.Second,
		}).Dial,
		TLSHandshakeTimeout: 10 * time.Second,
		MaxIdleConns:        100,
		MaxIdleConnsPerHost: 100,
	},
}

// SendHTTPRequest util function
func SendHTTPRequest(destinationURL, method string, headers map[string]string, jsonBody interface{}) func() {
	return func() {
		// create request body
		marshalled, err := json.Marshal(jsonBody)
		if err != nil {
			return
		}

		// create new request object
		request, err := http.NewRequest(method, destinationURL, bytes.NewBuffer(marshalled))
		if err != nil {
			return
		}

		// create headers
		request.Header.Set("Content-Type", "application/json")
		for key, value := range headers {
			request.Header.Set(key, value)
		}

		// run the http request
		resp, err := netClient.Do(request)
		if err != nil {
			return
		}
		defer resp.Body.Close()
	}
}

// WriteToFile util func
func WriteToFile(filename, content string) func() {
	return func() {
		f, err := os.Create(filename)
		if err != nil {
			if !strings.Contains(err.Error(), "no such file or directory") {
				return
			}
			split := strings.Split(filename, "/")
			if len(split) > 1 {
				os.MkdirAll(strings.Join(split[0:len(split)-1], "/"), os.ModePerm)
			}
			f, err = os.Create(filename)
			if err != nil {
				return
			}
		}
		defer f.Close()
		f.WriteString(content)
	}
}
