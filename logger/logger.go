package logger

import (
	"github.com/sirupsen/logrus"
)

// ILogable interface
type ILogable interface {
	Trace(args ...interface{})
	Debug(args ...interface{})
	Info(args ...interface{})
	Warn(args ...interface{})
	Error(args ...interface{})
	Fatal(args ...interface{})
	Panic(args ...interface{})
}

// Logger custom struct
type Logger struct {
	logable ILogable
}

// New constructor to init logger
func New(logable ILogable) *Logger {
	if logable == nil {
		logable = createDefaultLogable()
	}
	return &Logger{logable}
}

func createDefaultLogable() ILogable {
	lrus := logrus.New()
	lrus.SetLevel(logrus.TraceLevel)
	lrus.SetFormatter(&logrus.TextFormatter{
		DisableColors: false,
		ForceColors:   true,
		FullTimestamp: true,
	})
	return lrus
}

func runProcess(command func()) {
	if command != nil {
		command()
	}
}

// Trace func
func (log *Logger) Trace(args ...interface{}) {
	log.logable.Trace(args...)
}

// Debug func
func (log *Logger) Debug(args ...interface{}) {
	log.logable.Debug(args...)
}

// Info func
func (log *Logger) Info(args ...interface{}) {
	log.logable.Info(args...)
}

// Warn func
func (log *Logger) Warn(args ...interface{}) {
	log.logable.Warn(args...)
}

// Error func
func (log *Logger) Error(preProcess func(), postProcess func(), args ...interface{}) {
	runProcess(preProcess)
	log.logable.Error(args...)
	runProcess(postProcess)
}

// Fatal func
func (log *Logger) Fatal(preProcess func(), postProcess func(), args ...interface{}) {
	runProcess(preProcess)
	log.logable.Fatal(args...)
	runProcess(postProcess)
}

// Panic func
func (log *Logger) Panic(preProcess func(), postProcess func(), args ...interface{}) {
	runProcess(preProcess)
	log.logable.Panic(args...)
	runProcess(postProcess)
}
